FROM registry.gitlab.com/forestcitylabs/ops/php-base:latest

# Create a small shell script for executing console commands.
RUN curl -L https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar -o /usr/local/bin/phpcs
RUN curl -L https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar -o /usr/local/bin/phpcbf
RUN chmod a+x /usr/local/bin/phpcs
RUN chmod a+x /usr/local/bin/phpcbf
